execute if score #random word matches 36 run function common:words/36
execute if score #random word matches 35 run function common:words/35
execute if score #random word matches 34 run function common:words/34
execute if score #random word matches 33 run function common:words/33
execute if score #random word matches 32 run function common:words/32
execute if score #random word matches 31 run function common:words/31
execute if score #random word matches 30 run function common:words/30
execute if score #random word matches 29 run function common:words/29
execute if score #random word matches 28 run function common:words/28
execute if score #random word matches 27 run function common:words/27
execute if score #random word matches 26 run function common:words/26
execute if score #random word matches 25 run function common:words/25
execute if score #random word matches 24 run function common:words/24
execute if score #random word matches 23 run function common:words/23
execute if score #random word matches 22 run function common:words/22
execute if score #random word matches 21 run function common:words/21
execute if score #random word matches 20 run function common:words/20
execute if score #random word matches 19 run function common:words/19
execute if score #random word matches 18 run function common:words/18
execute if score #random word matches 17 run function common:words/17
execute if score #random word matches 16 run function common:words/16
execute if score #random word matches 15 run function common:words/15
execute if score #random word matches 14 run function common:words/14
execute if score #random word matches 13 run function common:words/13
execute if score #random word matches 12 run function common:words/12
execute if score #random word matches 11 run function common:words/11
execute if score #random word matches 10 run function common:words/10
execute if score #random word matches 9 run function common:words/9
execute if score #random word matches 8 run function common:words/8
execute if score #random word matches 7 run function common:words/7
execute if score #random word matches 6 run function common:words/6
execute if score #random word matches 5 run function common:words/5
execute if score #random word matches 4 run function common:words/4
execute if score #random word matches 3 run function common:words/3
execute if score #random word matches 2 run function common:words/2
execute if score #random word matches 1 run function common:words/1


effect give @e[type=iron_golem] minecraft:instant_damage 255 1
function common:check
scoreboard players remove #time time 1
execute if score #time time matches ..0 run function common:clock
execute store result bossbar time value run scoreboard players get #time time
scoreboard players add #fake 5s 1
execute if score #fake 5s matches 250.. run function common:orerefresh
execute as @a store result score @s ore run clear @s iron_ore 1
give @a[scores={ore=1..}] iron_block{CanPlaceOn:["minecraft:oak_planks","minecraft:end_stone_bricks","minecraft:iron_block"],HideFlags:127}
execute as @a store result score @s pump run clear @s pumpkin 1
give @a[scores={pump=1..}] carved_pumpkin{CanPlaceOn:["minecraft:iron_block"],HideFlags:127}
execute as @a store result score @s anvil run clear @s anvil 1
give @a[scores={anvil=1..}] anvil{CanPlaceOn:["minecraft:smooth_quartz"],HideFlags:127} 1
clear @a[tag=craft] pumpkin
clear @a[tag=craft] iron_block
clear @a[tag=craft] iron_ingot
clear @a[tag=craft] name_tag
execute if score #fake bingogamer = #fake players run function common:skip
tellraw @a[tag=!old] [{"text":"新年快乐，","color":"red"},{"selector":"@a[tag=!old]","bold":true,"color":"gold"}]
execute as @a[tag=!old] run tellraw @a {"text":"确定人齐后点击这里来开始","bold":true,"underlined":true,"color":"red","clickEvent":{"action":"run_command","value":"/function common:round"}}
xp set @a[tag=!old] 2021 levels
execute as @a[tag=!old] run gamemode adventure @s
tag @a add old
execute as @a if score @s death > @s deathP run tag @s add died
execute as @a[tag=died] store result score @s died run replaceitem entity @s hotbar.0 iron_sword{display:{Name:'{"text":"全村最好的剑","color":"gold"}',Lore:['{"text":"你就是达拉崩吧?"}']},HideFlags:102,Unbreakable:1b,Enchantments:[{id:"minecraft:sharpness",lvl:1s},{id:"minecraft:vanishing_curse",lvl:1s}]} 1
execute as @a[tag=died] store result score @s died run replaceitem entity @s hotbar.1 iron_pickaxe{CanDestroy:["minecraft:iron_ore","minecraft:iron_block"],display:{Name:'{"text":"镐子罢了","color":"white"}',Lore:['{"text":"这伤害颠覆了我的认知"}']},HideFlags:102,Unbreakable:1b,Enchantments:[{id:"minecraft:vanishing_curse",lvl:1s}]} 1
execute as @a[tag=died] store result score @s died run replaceitem entity @s hotbar.2 iron_axe{CanDestroy:["minecraft:pumpkin","minecraft:carved_pumpkin"],display:{Name:'{"text":"普通的斧子"}',Lore:['{"text":"确实，毕竟就这点伤害"}']},HideFlags:102,Enchantments:[{id:"minecraft:vanishing_curse",lvl:1s}],Unbreakable:1b,Damage:0.1,AttributeModifiers:[{AttributeName:"generic.attack_damage",Name:"generic.attack_damage",Amount:-4,Operation:0,UUID:[I;-1568137819,-1840496223,-1926552039,-217150947]}]} 1
execute as @a[tag=died] store result score @s died run replaceitem entity @s hotbar.3 apple{display:{Name:'{"text":"平安果","color":"gold","bold":true,"italic":false}',Lore:['{"text":"新春快乐！","color":"red","italic":false}']},HideFlags:127,Enchantments:[{id:"minecraft:vanishing_curse",lvl:1s}]} 32
execute as @a[tag=died] if score @s died matches 1.. run scoreboard players operation @s deathP = @s death
execute as @a[tag=died] if score @s died matches 1.. run tag @s remove died