import os
import shutil
import xlrd

#问题获取
path = os.path.abspath(os.path.dirname(os.getcwd()))
print(path)
list_file = path+"\\q-a\\list.xls"
file = xlrwb = xlrd.open_workbook(filename=list_file)
worksp = file.sheet_by_name("问题")
questions = worksp.col_values(0)
del questions[0]
print("当前列表:")
print(questions)

a = input("输入Y确认")
if a != "Y":
    os._exit()

#删除原有内容
try:
    shutil.rmtree(path+'\\words')
except:
    pass
try:    
    os.mkdir(path+'\\words')
    print("已清空游戏内部列表\n")
except:
    pass
#加载模板
template_file = open(path+r"\q-a\template","r",encoding = 'utf-8')
template=template_file.read()
template_file.close

template_file2 = open(path+r"\q-a\template2","r",encoding = 'utf-8')
template2=template_file2.read()
template_file2.close

shutil.copy(path+r"\q-a\template3",path+r"\tick.mcfunction")
shutil.copy(path+r"\q-a\template4",path+r"\round.mcfunction")
shutil.copy(path+r"\q-a\template5",os.path.abspath(os.path.join(os.getcwd(), "../.."))+"\\loot_tables\\random.json")
count = 1
for i in questions:
    try:
        i = str(i)
        print("正在添加"+i)
        context_file = open(path+"\\words\\%d.mcfunction"%count,"w",encoding = 'utf-8')#配置判定文件
        context = list(template)
        context.insert(49,"\"%s\""%i)
        context_file.write("".join(context))
        context_file.close()

        context_file = open(path+"\\words\\%db.mcfunction"%count,"w",encoding = 'utf-8')#配置开始文件
        context = list(template2)
        context.insert(66,i)
        context.insert(164,i)
        context_file.write("".join(context))
        context_file.close()

        context_file = open(path+"\\tick.mcfunction","r",encoding = 'utf-8') #修改链接文本tick
        context = context_file.read()
        context_file.close()
        context = "execute if score #random word matches %d run function common:words/%d\n"%(count,count)+context
        context_file = open(path+"\\tick.mcfunction","w",encoding = 'utf-8')
        context_file.seek(0,0)
        context_file.write(context)
        context_file.close()


        context_file = open(path+"\\round.mcfunction","r",encoding = 'utf-8') #修改链接文本round
        context = context_file.readlines()
        context_file.close()
        
        context_file = open(path+"\\round.mcfunction","w",encoding = 'utf-8') 
        context.insert(29,"execute if score #random word matches %d run function common:words/%db\n"%(count,count))
        context_file.write("".join(context))
        context_file.close()

        print("你已成功添加"+i)
    except:
        print("添加 %s 失败"%i)
    
    count+=1
context_file = open(os.path.abspath(os.path.join(os.getcwd(), "../.."))+"\\loot_tables\\random.json","r",encoding = 'utf-8') #配置随机范围
context = context_file.readlines()
context_file.close()
context_file = open(os.path.abspath(os.path.join(os.getcwd(), "../.."))+"\\loot_tables\\random.json","w",encoding = 'utf-8')
context[21] = "\"max\": %d\n"%(count+1)
context_file.writelines(context)
context_file.close()

stop = input("\n已完成，回车退出")    
