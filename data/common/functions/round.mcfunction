clear @a[tag=craft]
tag @a[tag=crafted] remove craft
execute store result score #fake word run tag @r[tag=!crafted] add craft
clear @a[tag=craft]
execute if score #fake word matches 0 run tag @a remove crafted
tag @a[tag=craft] add crafted
execute if score #fake round matches 20 run title @a title {"text":"游戏结束！","color":"red"}
execute if score #fake round matches 20 run datapack disable "HappyChineseNewYear"
scoreboard players set #fake players 0
execute as @a run scoreboard players add #fake players 1
scoreboard players set #time time 3600
scoreboard players add #fake round 1
title @a subtitle [{"text":"第","color":"aqua"},{"score":{"name":"#fake","objective":"round"},"bold":true,"color":"yellow"},{"text":"轮","color":"aqua"}]
setblock 370 255 -370 chest
loot insert 370 255 -370 loot common:random
execute store result score #random word run data get block 370 255 -370 Items[0].tag.AttributeModifiers[0].Amount
setblock 370 255 -370 air
scoreboard players add #fake word 1
execute if entity @a[tag=craft] run tellraw @a [{"text":"下一轮建造者是:","color":"white"},{"selector":"@a[tag=craft]","bold":true,"color":"aqua"}]
execute unless entity @a[tag=craft] run tellraw @a {"text":"自由pvp！","color":"red","bold":"true"}
tp @p[tag=craft] 378.5 64 -382.5
gamemode adventure @a
gamemode creative @a[tag=craft]






execute if score #random word matches 36 run function common:words/36b
execute if score #random word matches 35 run function common:words/35b
execute if score #random word matches 34 run function common:words/34b
execute if score #random word matches 33 run function common:words/33b
execute if score #random word matches 32 run function common:words/32b
execute if score #random word matches 31 run function common:words/31b
execute if score #random word matches 30 run function common:words/30b
execute if score #random word matches 29 run function common:words/29b
execute if score #random word matches 28 run function common:words/28b
execute if score #random word matches 27 run function common:words/27b
execute if score #random word matches 26 run function common:words/26b
execute if score #random word matches 25 run function common:words/25b
execute if score #random word matches 24 run function common:words/24b
execute if score #random word matches 23 run function common:words/23b
execute if score #random word matches 22 run function common:words/22b
execute if score #random word matches 21 run function common:words/21b
execute if score #random word matches 20 run function common:words/20b
execute if score #random word matches 19 run function common:words/19b
execute if score #random word matches 18 run function common:words/18b
execute if score #random word matches 17 run function common:words/17b
execute if score #random word matches 16 run function common:words/16b
execute if score #random word matches 15 run function common:words/15b
execute if score #random word matches 14 run function common:words/14b
execute if score #random word matches 13 run function common:words/13b
execute if score #random word matches 12 run function common:words/12b
execute if score #random word matches 11 run function common:words/11b
execute if score #random word matches 10 run function common:words/10b
execute if score #random word matches 9 run function common:words/9b
execute if score #random word matches 8 run function common:words/8b
execute if score #random word matches 7 run function common:words/7b
execute if score #random word matches 6 run function common:words/6b
execute if score #random word matches 5 run function common:words/5b
execute if score #random word matches 4 run function common:words/4b
execute if score #random word matches 3 run function common:words/3b
execute if score #random word matches 2 run function common:words/2b
execute if score #random word matches 1 run function common:words/1b

fill 352 64 -358 402 74 -408 air
fill 352 74 -358 402 84 -408 air
fill 352 84 -358 402 93 -408 air

kill @e[type=!player,type=!zombie]
replaceitem entity @a[tag=!craft] hotbar.0 iron_sword{display:{Name:'{"text":"全村最好的剑","color":"gold"}',Lore:['{"text":"你就是达拉崩吧?"}']},HideFlags:102,Unbreakable:1b,Enchantments:[{id:"minecraft:sharpness",lvl:1s},{id:"minecraft:vanishing_curse",lvl:1s}]} 1
replaceitem entity @a[tag=!craft] hotbar.1 iron_pickaxe{CanDestroy:["minecraft:iron_ore","minecraft:iron_block"],display:{Name:'{"text":"镐子罢了","color":"white"}',Lore:['{"text":"这伤害颠覆了我的认知"}']},HideFlags:102,Unbreakable:1b,Enchantments:[{id:"minecraft:vanishing_curse",lvl:1s}]} 1
replaceitem entity @a[tag=!craft] hotbar.2 iron_axe{CanDestroy:["minecraft:pumpkin","minecraft:carved_pumpkin"],display:{Name:'{"text":"普通的斧子"}',Lore:['{"text":"确实，毕竟就这点伤害"}']},HideFlags:102,Enchantments:[{id:"minecraft:vanishing_curse",lvl:1s}],Unbreakable:1b,Damage:0.1,AttributeModifiers:[{AttributeName:"generic.attack_damage",Name:"generic.attack_damage",Amount:-4,Operation:0,UUID:[I;-1568137819,-1840496223,-1926552039,-217150947]}]} 1
replaceitem entity @a[tag=!craft] hotbar.3 apple{display:{Name:'{"text":"平安果","color":"gold","bold":true,"italic":false}',Lore:['{"text":"新春快乐！","color":"red","italic":false}']},HideFlags:127,Enchantments:[{id:"minecraft:vanishing_curse",lvl:1s}]} 32
replaceitem entity @a[tag=!craft] armor.chest minecraft:iron_chestplate
replaceitem entity @a[tag=!craft] armor.feet iron_boots
replaceitem entity @a[tag=!craft] armor.head iron_helmet
replaceitem entity @a[tag=!craft] armor.legs minecraft:iron_leggings
bossbar set minecraft:time players @a