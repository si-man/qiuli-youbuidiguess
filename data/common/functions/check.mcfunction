execute at @e[type=iron_golem,tag=bingo] run scoreboard players add @p score 1
execute at @e[type=iron_golem,tag=bingo] run effect give @p instant_health 1 1 false
execute at @e[type=iron_golem,tag=bingo] run particle minecraft:heart ~ ~0.5 ~ 1.5 0.2 1.5 1 30 normal
execute at @e[type=iron_golem,tag=bingo] run gamemode spectator @p
execute at @e[type=iron_golem,tag=bingo] run tellraw @a ["",{"selector":"@p","bold":true,"color":"gold"},{"text":"猜中了答案！","color":"yellow"}]
execute as @e[type=iron_golem,tag=bingo] run tag @p add bingogamer
execute at @e[type=iron_golem,tag=bingo] run scoreboard players add @a[tag=craft] score 1
kill @e[type=iron_golem,tag=bingo]
