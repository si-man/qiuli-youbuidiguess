# 秋梨你建我猜

#### 介绍
秋梨社mc地图 你建我猜
题目在xls格式的题库中自行添加，使用main进行自动打包
仅在windows-x86-64bit系统进行测试 mc版本java-1.16.x

#### [秋梨社](http://https://space.bilibili.com/308020237)


#### 安装教程

1.  下载最新发行版
2.  导入地图至.minecraft/**/saves, **修改题库** /datapacks/qiuli-youbuildiguess/data/common/functions/q-a/list.xls
3.  运行main.exe/main.py 按照提示生成文件（请确保程序拥有读写权限）

[视频教程、试玩](https://www.bilibili.com/video/BV1yA411T7K8)
#### 游戏规则
0.按照安装教程创造题库生成文件

1.每回合3分钟 在开始时选出一位建筑师，其余为猜测者

2.建筑师：

    （1）在规定时间内在中央50*50区域按题目建造

    （2）尽量不要直接绘制文字符号等

    （3）可以通过语音或交流区稍稍提示
 
 猜测者:

    （1）收集足够对铁傀儡命名的材料（铁矿-铁块，南瓜-雕刻南瓜，僵尸-命名牌）

    （2）回到小木屋制作铁砧，修改命名牌名称为你的答案，制作铁傀儡并对他命名（铁傀儡只能存在5s哦）


3.猜中猜测者建筑师各得一分

4.被选中猜测者会清空背包哦~

5.PVP要素弱化，但是你愿意的话可以~

6.暂时无法脱离本地图使用

#### 如有bug等，请提交issue或联系bxhsiman@gmail.com
